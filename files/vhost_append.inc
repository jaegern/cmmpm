  location /reports/report.html {
    auth_basic "Restricted";
    auth_basic_user_file /usr/local/nginx/reports/dummydomain/.htpasswd;
    autoindex on;
}

#!/bin/bash

# Centminmod PostMortem
# Script for helping with additional stuff from centminmod

### Fancy stuff
BLACK='\E[30m'
RED='\E[31m'    # Warnings
GREEN='\E[32m'  # Success
YELLOW='\E[33m' # Info
BLUE='\E[34m'   # Input
MAGENTA='\E[35m'
CYAN='\E[36m'
WHITE='\E[37m'

boldblack='\E[1;30m'
boldred='\E[1;31m'
boldgreen='\E[1;32m'
boldyellow='\E[1;33m'
boldblue='\E[1;34m'
boldmagenta='\E[1;35m'
boldcyan='\E[1;36m'
boldwhite='\E[1;37m'

CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"

RESET="\033[00m" # \e[00m

Reset="tput sgr0"
cecho() { # Coloured-echo.
	# Argument $1 = message
	# Argument $2 = color
	message=$1
	color=$2
	echo -e "$color$message"
	$Reset
	return
}

spinner() {
	bar=" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	barlength=${#bar}
	i=0
	while ((i < 100)); do
		n=$((i * barlength / 100))
		printf "\e[00;34m\r[%-${barlength}s]\e[00m" "${bar:0:n}"
		((i += RANDOM % 5 + 2))
		sleep 0.02
	done
}

##### Acme Tool
function _acme() {
	cfg_path=/etc/centminmod/custom_config.inc
	if [ -e "$cfg_path" ] && grep -qF "LETSENCRYPT_DETECT='y'" "$cfg_path"; then
		echo "Lets Encrypt entry present...Skipping"
	else
		echo "LETSENCRYPT_DETECT='y'" >>"$cfg_path"
		echo "Added ACME Tool...Done!"
	fi
	spinner
	#--- Executing ACME Installation
	cd /usr/local/src/centminmod/addons
	./acmetool.sh acmeinstall

}

##### Maldet
function _maldet() {
	#mldt_file=/usr/local/src/centminmod/addons/maldet.sh
	mldt_dir=/usr/local/src/centminmod/addons
	read -ep "Please input your E-Mail: " mdMail
	sed -i "s/ALERTEMAIL=.*/ALERTEMAIL='$mdMail'/" "$mldt_dir/maldet.sh"

	#--- Executing maldet.sh
	cd $mldt_dir/maldet.sh
	./maldet.sh

	# TODO: Output example scan: maldet -a /home/nginx/domains/domain.com/public
	# Bash alias? mdscan
	read -ep "Do you want to extend ClamAV signatures? (y/n) " extendSigs
	if [[ $extendSigs == [yY] ]]; then
		spinner
		cat >/etc/freshclam.conf <<EOF
DatabaseCustomURL http://cdn.malware.expert/malware.expert.ndb
DatabaseCustomURL http://cdn.malware.expert/malware.expert.hdb
DatabaseCustomURL http://cdn.malware.expert/malware.expert.ldb
DatabaseCustomURL http://cdn.malware.expert/malware.expert.fp
DatabaseCustomURL http://www.rfxn.com/downloads/rfxn.ndb
DatabaseCustomURL http://www.rfxn.com/downloads/rfxn.hdb
EOF
	fi
}

##### PHPmyAdmin
function _phpmyadmin() {
	phpma_file=/usr/local/src/centminmod/addons/phpmyadmin.sh
	cd /usr/local/src/centminmod/addons
	wget --no-check-certificate https://github.com/centminmod/phpmyadmin/raw/master/phpmyadmin.sh
	chmod 0700 /usr/local/src/centminmod/addons/phpmyadmin.sh
	read -ep "Do you have a static IP (y/n): " staticIP
	if [[ "$staticIP" == [yY] ]]; then
		spinner
		sed -i "s/STATICIP=.*/STATICIP='y'/" "$phpma_file"
	fi
	#--- TODO: Check if Port is open and open it
	grep -F "TCP_IN" /etc/csf/csf.conf | grep "9418"
}

function _mysqladmin() {
	if [[ ! -d /root/tools ]]; then
		mkdir -p /root/tools
	else
		cd /root/tools/
		wget -O mysqladmin_shell.sh https://github.com/centminmod/centminmod/raw/123.09beta01/addons/mysqladmin_shell.sh
		spinner
		chmod +x mysqladmin_shell.sh
		#--- Make it global executable
		cp mysqladmin_shell.sh /usr/bin/mysqladmin_shell
		chmod +x /usr/bin/mysqladmin_shell
	fi
}

# ##### Harden SSH
# function _harden_ssh() {
# 	#--- Check if CMM is installed
# 	if [ ! -f /usr/local/src/centminmod/centmin.sh ]; then
# 		echo -e "Hardening ${RED}FAILED${RESET}, please be sure you have centminmod installed!"
# 	fi

# 	#--- Variables
# 	ssh_conf="/etc/ssh/ssh_config"
# 	sshd_conf="/etc/ssh/sshd_config"
# 	#--- Backing up current configs
# 	cp $ssh_conf{,.bak}
# 	cp $sshd_conf{,.bak}
# 	#--- Start hardening
# 	read -ep "Create new user? (y/n)" newUser
# 	if [[ "$newUser" == [yY] ]]; then
# 		read -ep" Type the new username: " username
# 		adduser $username
# 		usermod -aG wheel $username
# 		serverip=$(__get_ip)
# 		mkdir -p /home/$username/.ssh
# 		touch /home/$username/authorized_keys
# 		read -ep "     a) ssh-keygen -t ed25519 " foo1
# 		read -ep "     b) cat /home/$username/.ssh/id_ed25519.pub >> /home/$username/.ssh/authorized_keys: " foo2

# 		#--- Adding protocol 2 / Needed?
# 		sed -i '/#ListenAddress ::/a \
# Protocol 2' $sshd_conf

# 		#--- Harden
# 		sed -i -e 's/^.*PermitRootLogin.*$/PermitRootLogin no/' $sshd_conf
# 		sed -i -e 's/^.*PermitEmptyPasswords.*$/PermitEmptyPasswords no/' $sshd_conf
# 		sed -i -e 's/^.*PasswordAuthentication.*$/PasswordAuthentication no/' $sshd_conf
# 		sed -i -e 's/^.*PubkeyAuthentication.*$/PubkeyAuthentication yes/' $sshd_conf

# 		service sshd restart && service ssh restart
# 	fi
# }

# Fix colors with weird backgrounds
function _fixcolors() {
	cp -a /usr/local/src/centminmod /usr/local/src/centminmod-backup
	find /usr/local/src/centminmod -type f \( -name '*.inc' -o -name '*.sh' \) -print | while read f; do
		echo $f
		sed -i 's/;40m/m/g' $f
	done
}

function _revertcolors() {
	echo "Only works if you have used the 'Fix Colors'"
	if [[ ! -d /usr/local/src/centminmod-backup ]]; then
		echo "Backup directory not found...skipping"
	else
		cp -a /usr/local/src/centminmod-backup /usr/local/src/centminmod
	fi
}

function _goaccess() {
	# ---Check if Goaccess is already present. If no, we install
	if [[ ! -f /usr/local/bin/goaccess ]]; then
		wget https://tar.goaccess.io/goaccess-1.2.tar.gz
		tar -xzvf goaccess-1.2.tar.gz
		cd goaccess-1.2/
		./configure --enable-utf8 --enable-geoip=legacy
		make
		make install
	fi

	#--- Setup directories
	read -ep "What is your domain? (e.g.: example.com)  " gadomain
	mkdir -p /home/nginx/domains/$gadomain/public/reports

	#--- Check for existing .htpasswd and remove if present
	if [[ -f /usr/local/nginx/reports/$gadomain/.htpasswd ]]; then
		rm -f /usr/local/nginx/reports/$gadomain/.htpasswd
	fi

	#--- Setup .htpasswd
	mkdir -p /usr/local/nginx/reports/$gadomain
	read -ep "Desired Username: " gauser
	echo -n "$gauser:" >/usr/local/nginx/reports/$gadomain/.htpasswd
	openssl passwd -apr1 >>/usr/local/nginx/reports/$gadomain/.htpasswd

	#--- Configure config

	if grep -Fxq "time-format %T" /usr/local/etc/goaccess.conf; then
		echo "Config already setup..."
	else
		cat >/usr/local/etc/goaccess.conf <<EOF
time-format %T
date-format %d/%b/%Y
log_format %h - %^ [%d:%t %^] "%r" %s %b "%R" "%u"
config-dialog false
hl-header true
json-pretty-print false
no-color false
no-column-names false
no-csv-summary false
no-progress false
no-tab-scroll false
with-mouse false
agent-list false
with-output-resolver false
http-method yes
http-protocol yes
no-query-string false
no-term-resolver false
444-as-404 false
4xx-to-unique-count false
all-static-files false
double-decode false
ignore-crawlers false
crawlers-only false
ignore-panel REFERRERS
ignore-panel KEYPHRASES
real-os true
static-file .css
static-file .js
static-file .jpg
static-file .png
static-file .gif
static-file .ico
static-file .jpeg
static-file .pdf
static-file .txt
static-file .csv
static-file .zip
static-file .mp3
static-file .mp4
static-file .mpeg
static-file .mpg
static-file .exe
static-file .swf
static-file .woff
static-file .woff2
static-file .xls
static-file .xlsx
static-file .doc
static-file .docx
static-file .ppt
static-file .pptx
static-file .iso
static-file .gz
static-file .rar
static-file .svg
static-file .bmp
static-file .tar
static-file .tgz
static-file .tiff
static-file .tif
static-file .ttf
static-file .flv
EOF
	fi
# 	spinner
# 	#--- First report and cron setup
# 	if [[ -f /home/nginx/domains/$gadomain/log/*.gz ]]; then
# 		/usr/bin/zcat -f /home/nginx/domains/$gadomain/log/access.log-* | /usr/local/bin/goaccess -p /usr/local/etc/goaccess.conf -a >/home/nginx/domains/$gadomain/public/reports/report.html
# 		cat >/etc/cron.daily/$gadomain <<EOF
# #!/bin/bash
# /usr/bin/zcat -f /home/nginx/domains/$gadomain/log/access.log-* | /usr/local/bin/goaccess -p /usr/local/etc/goaccess.conf -a > /home/nginx/domains/$gadomain/public/reports/report.html -
# EOF
# 	else
# 		/usr/local/bin/goaccess -f /home/nginx/domains/$gadomain/log/access.log -a >/home/nginx/domains/$gadomain/public/reports/report.html
# 		cat >/etc/cron.daily/$gadomain <<EOF
# #!/bin/bash
# /usr/local/bin/goaccess -f /home/nginx/domains/$gadomain/log/access.log -a > /home/nginx/domains/$gadomain/public/reports/report.html -
# EOF
# 	fi
# 	sed -i "s/dummydomain/$gadomain/g" /root/tools/cmmpm/files/vhost_append.inc
# 	read -ep "Is this Domain running with Wordpress? (y/n) " iswp
# 	if [[ $iswp == [yY] ]]; then
# 		sed -i "/include /usr/local/nginx/conf/php-rediscache.conf/{N;N;r /root/tools/cmmpm/files/vhost_append.inc
# }" /usr/local/nginx/conf/conf.d/$gadomain.ssl.conf /usr/local/nginx/conf/conf.d/$gadomain.conf
# 	else
# 		sed -i "/include fastcgi_params/{N;N;r /root/tools/cmmpm/files/vhost_append.inc
# }" /usr/local/nginx/conf/conf.d/$gadomain.ssl.conf /usr/local/nginx/conf/conf.d/$gadomain.conf
# 	fi

# 	#--- Revert $gadomain back to dummydomain - so dirty..
# 	sed -i "s/$gadomain/dummydomain/g" /root/tools/cmmpm/files/vhost_append.inc
}

#--- MysqlMyMon
function _mysqlmymon() {
	if [[ ! -d /root/tools ]]; then
		mkdir -p /root/tool
	fi

	if [[ ! -f /root/tools/mysqlmymonlite.sh ]]; then
		cd /root/tools
		rm -rf /root/mysqltuner.pl
		rm -rf mysqlmymonlite.sh
		wget http://mysqlmymon.com/download/mysqlmymonlite.zip
		unzip -o mysqlmymonlite.zip
		mv centos/mysqlmymonlite.sh .
		rm -rf mysqlmymonlite.zip centos centos_whm debian changelog*
		chmod +x mysqlmymonlite.sh
	else
		echo "MysqlMyMon found...skipping"
	fi
}

##### Menu
menu=""
until [ "$menu" = "10" ]; do
	clear
	echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
	echo -e "\e[93m[+]\e[00m Please choose a option"
	echo -e "\e[34m---------------------------------------------------------------------------------------------------------\e[00m"
	echo ""
	echo -e "1. Install ${GREEN}ACME${RESET} Tool"
	echo -e "2. Configure ${GREEN}Maldet${RESET}"
	echo -e "3. Install ${GREEN}PHPMYADMIN${RESET}"
	echo -e "4. Install ${GREEN}Mysqladmin Shell${RESET}"
	echo -e "5. Fix ${GREEN}colors${RESET}"
	echo -e "6. Revert ${GREEN}colors${RESET}"
	echo -e "7. Install ${GREEN}Goaccess${RESET}"
	echo -e "8. Install ${GREEN}MysqlMyMon${RESET}"
	echo "9. Exit"
	echo

	read menu
	case $menu in

	1)
		_acme
		;;
	2)
		_maldet
		;;
	3)
		_phpmyadmin
		;;
	4)
		_mysqladmin
		;;
	5)
		_fixcolors
		;;
	6)
		_revertcolors
		;;
	7)
		_goaccess
		;;
	8)
		_mysqlmymon
		;;
	9)
		break
		;;

	*) ;;

	esac
done
